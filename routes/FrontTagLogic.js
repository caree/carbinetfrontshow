/******************************************

目标：
1. 标签读取敏感度
> 设置敏感度系数
> 可以用来调试标签在货架上是否处于合适的位置，并给出合适的建议
> 当标签被读取到的次数大于等于敏感度系数时，认为读写器读取标签性能比较好，建议减少信息接收次数法确定标签的存在
> 当标签被读取到的次数普遍少于敏感度系数时，认为读写器不能稳定高质量的读取到标签，建议通过改善硬件环境或者提高接收次数标准来确定标签的存在
eg.  tag: 123456789 low -----



2. 误读
>实时报告误读的标签，作为调节外在环境的参考
eg.  tag: 123456789 mis ******


3. 能够设定根据读取敏感度和持续读取到的次数来决策标签是否存在
> 读取程度：由于读写器功率的强弱不同以及标签的性质和位置的不同，每次读写器发送的读取到的标签信息中读取次数可能不同，这标志着读写器对该标签存在感认同标准的不同，读取到次数从1开始
> 读写器每隔一定时间（默认设置为2s）发送一次读取到的标签，去除误读的因素，可能需要1~3次才能确定标签存在，因此这种读取次数的设置不同，也决定了读取确定的时间不同
> 在接收标签下次数据和最近次数据之间的间隔时间，该间隔时间决定着是否应该认为标签已经不存在，或者两次接收数据为连续数据
eg.  tag: 123456789 too lag @@@@@@

--------------------------------------------
4. 支持获取标签API和事件推送
> 客户端根据API获取当前标签状态
> 标签发生变化时，将变化事件推送到客户端

*******************************************/
//部署参数
// var maxTagReadInterval = 4000;//标签上次读到和这次读到时间的最长间隔，超过该间隔认为标签离开了读写器可读取范围
// var minTagReadedConfirmCount = 2;//要确定标签是否已经读到，这是最少的次数
// var minSensitiveCount = 1;//敏感度下限次数，低于该值则说明标签读取不够灵敏

//测试参数
// var listeningPort = 5100;// UDP端口
var maxTagReadInterval = 8000; //标签上次读到和这次读到时间的最长间隔，超过该间隔认为标签离开了读写器可读取范围
var minTagReadedConfirmCount = 3; //要确定标签是否已经读到，这是最少的次数
var minSensitiveCount = 1; //敏感度下限次数，低于该值则说明标签读取不够灵敏

var WebSocket = require('faye-websocket');
var dgram = require("dgram");
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var dataParser = require('./dataParser');

var ifPrintMisReading = true; //是否打印误读信息
var antenaIDScrope = ['01', '02', '04', '08']; //正确天线值，如果天线值为该数组中的一个，说明没有误读
var ifPrintSensitiveReport = true; //是否打印敏感度报告
var WebSocketEventCenter = null;

var parser = new dataParser({
    listeningPort: listeningPort,
    ep: ep
});
var tagList = parser.getTagList();
var lastExistsTagList = [];

parser.startServer();
setInterval(checkTagExist, 1000);
// createWebSocket();


//生成一个数组自身的阶差，例如 [15,9,7,6,1] 生成  [ 6, 2, 1, 5 ]
Array.prototype.mapMinusSelf = function() {
    if (this.length <= 1) return [];
    var mapMinus = [];
    for (var i = 0, len = this.length; i < len - 1; i++) {
        mapMinus.push(this[i] - this[i + 1]);
    }
    return mapMinus;
};

exports.getAllExistTags = function() {
    return getAllExistTags();
};


function getAllExistTags() {
    var uniqList = _.uniq(lastExistsTagList, false, function(_tag) {
        return _tag.epc;
    });
    var existTagList = _.map(uniqList, function(_tag) {
        return {
            epc: _tag.epc,
            antenaID: _tag.antenaID,
            Event: 'normal'
        };
    });
    return existTagList;
}

function checkTagExist() {
    console.log((timeFormater() + ' checkTagExist ...').data);

    var currentExistsTagList = getCurrentExistsTagList();
    // console.log('lastExistsTagList:');
    // console.dir(lastExistsTagList);
    // console.log('currentExistsTagList:');
    // console.dir(currentExistsTagList);
    //与上次的列表进行比较，可以发现变化的标签
    seeWhichTagChanged(currentExistsTagList, lastExistsTagList);

    lastExistsTagList = currentExistsTagList;
    return;
}

function seeWhichTagChanged(_currentExistsTagList, _lastExistsTagList) {
    // 切换天线的判断以及新出现和消失
    // 综合比较
    var lastExistsEpcs = _.pluck(_lastExistsTagList, "epc");
    var currentExistsEpcs = _.pluck(_currentExistsTagList, "epc");
    var epcsDisappear = _.difference(lastExistsEpcs, currentExistsEpcs);
    if (_.size(epcsDisappear) > 0) {
        var tagsDisappear = _.without(_.map(_lastExistsTagList, function(_tag) {
            if (_.contains(epcsDisappear, _tag.epc)) {
                _tag.Event = "deleted";
                console.log((_tag.epc + ' deleted').info);
                return _tag;
            } else {
                return null;
            }
        }), null);
        ep.emit('tagDeleted', tagsDisappear);
    }

    var epcsAppear = _.difference(currentExistsEpcs, lastExistsEpcs);
    if (_.size(epcsAppear) > 0) {
        var tagsAppear = _.without(_.map(_currentExistsTagList, function(_tag) {
            if (_.contains(epcsAppear, _tag.epc)) {
                _tag.Event = "added";
                console.log(('new tag ' + _tag.epc).info);
                return _tag;
            } else {
                return null;
            }
        }), null);
        ep.emit('tagAdded', tagsAppear);
    }

    //两次都存在，看是否天线有变化
    var epcsAlwaysExists = _.intersection(currentExistsEpcs, lastExistsEpcs);
    if (_.size(epcsAlwaysExists) > 0) {
        var tagsAntenaChanged = _.without(_.map(epcsAlwaysExists, function(_epc) {
            var lastTag = _.findWhere(_lastExistsTagList, {
                epc: _epc
            });
            var currentTag = _.findWhere(_currentExistsTagList, {
                epc: _epc
            });
            if (lastTag.antenaID != currentTag.antenaID) {
                currentTag.Event = 'changed';
                return currentTag;
            }
            return null;
        }), null);
        ep.emit('tagAntenaChanged', tagsAntenaChanged);
    }

}

function getCurrentExistsTagList() {
    // console.log('getCurrentExistsTagList ...');
    var currentTimeStamp = (new Date()).getTime();
    var currentExistsTagList = _.without(_.map(tagList, function(_tag) {
        var records = _tag.records;
        // 至少有多次的读取记录才能参与是否真正读取的选择
        if (_.size(records) <= minTagReadedConfirmCount) {
            console.info((_tag.epc + ': no enough records').data);
            return null;
        }

        if (_.every(_.first(records, minTagReadedConfirmCount), function(_record) {
            //todo: 以及偶尔误读的判断

            //排除误读，最近几次都不能出现误读,排除读取次数太少的标签
            if (_.contains(antenaIDScrope, _record.antenaID) === false) {
                console.log((_record.epc + ' misreading!!!').data);
                return false;
            }
            if (_record.readCount < minSensitiveCount) {
                console.log((_record.epc + ' too less read!!!').data);
                return false;
            }

            //最后通过读取的间隔时间确定
            return true;
        }) === false) {
            // console.info(_tag.epc + ': ')
            return null;
        }

        // 获取时间序列，评判时间间隔
        var timeStamps = _.pluck(records, 'timeStamp');
        timeStamps.unshift(currentTimeStamp); //插入当前时间，为生成阶差做准备
        var mapMinus = _.initial(timeStamps.mapMinusSelf(), minTagReadedConfirmCount);
        var truePredictMap = _.map(mapMinus, function(_minusResult) {
            return _minusResult <= maxTagReadInterval;
        });
        // console.dir(truePredictMap);
        if (_.findWhere(lastExistsTagList, {
            epc: _tag.epc
        }) === null) {
            //原来标签不存在，只有一个大间隔还可以接受
            if (_.size(_.without(truePredictMap, true)) > 1) {
                console.info((_tag.epc + ':  too many large time interval, should NOT added!!!').warn);
                return null;
            }
        } else {
            //如果标签之前已经存在，只要有一个正常的就可以接受
            if (_.size(_.without(truePredictMap, true)) >= minTagReadedConfirmCount) {
                console.info((_tag.epc + ':  too many large time interval, should DELETED!!!').warn);
                return null;
            }
        }
        if (_.contains(_.map(mapMinus, function(_minusResult) {
            return _minusResult >= (maxTagReadInterval);
        }), true)) {
            console.log((_tag.epc + ':  there is a too large interval !!!!').warn);
            console.dir(mapMinus);
            return null;
        }

        return {
            epc: _tag.epc,
            antenaID: _tag.records[0].antenaID
        };
    }), null);
    return currentExistsTagList;
}


function createWebSocket() {
    console.log(('createWebSocket =>').data);
    WebSocketEventCenter = new WebSocket.Client(wsUriEventCenter);
    WebSocketEventCenter.on('open', function (evt) { onOpen(evt) });
    WebSocketEventCenter.on('close', function (evt) { onClose(evt) });
    WebSocketEventCenter.on('message', function (evt) 
    { 
        onMessage(evt) 
    });
    WebSocketEventCenter.on('error', function (evt) { onError(evt) });
}
function onOpen(evt) {
    console.log(('WebSocketEventCenter open => ').data);
    // var regCmd = {name:'newProductionOrder', msgType:'reg', para:''};
    // WebSocketEventCenter.send(JSON.stringify(regCmd));
    // var regCmd = {name:'newEpcOnProduction', msgType:'reg', para:''};
    // WebSocketEventCenter.send(JSON.stringify(regCmd));         
    // var regCmd = {name:'acceptNewEpc', msgType:'reg', para:''};
    // WebSocketEventCenter.send(JSON.stringify(regCmd));         

}
function onClose(evt) {
    console.log(('WebSocketEventCenter closed!').info)
    WebSocketEventCenter = null;
}
function onError(evt){
    console.log(('WebSocketEventCenter error!!!').error);
    WebSocketEventCenter = null;
}
function onMessage(evt) {
    // console.log('ws onMessage => ');
}


