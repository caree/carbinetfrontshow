
var request = require('request');
var EventProxy = require('eventproxy');
var _ = require("underscore");


exports.carbinetFront = function(req, res){
	res.render('carbinetFront', {title: '智能货架'});
};
exports.carbinetFront2 = function(req, res){
	res.render('carbinetFront2', {title: '智能货架'});
};
exports.carbinetFront3 = function(req, res){
	res.render('carbinetFront3', {title: '智能货架'});
};
exports.carbinetFront4 = function(req, res){
	var typeImageMapList = [{itemType:'01', image:'xbox.png'}, {itemType:'null', image:'box1.png'}, 
							{itemType:'default', image:'default.png'}, {itemType:'02', image:'mac.png'}];

	var ep = EventProxy.create('listEpc', 'listTypeInfo', function(_epcTypeList, _typeInfoList){
	
		var typeImageMap = _.map(JSON.parse(_typeInfoList), function(_typeInfo){
			var imageMap = _.find(typeImageMapList, function(_map){
				return _map.itemType == _typeInfo.typeCode;
			});
			if(imageMap == null) imageMap = {itemType:'default', image:'default.png'};
			return {itemType: _typeInfo.typeCode, title: _typeInfo.typeName, des: _typeInfo.des, image: imageMap.image};
		});//需要序列化之后给模板
		typeImageMap.push({itemType:'default', image:'default.png', des:'这个位置放置的东西还没有制定种类！', title:'未知'});
		typeImageMap.push({itemType:'null', image:'box1.png', des:'这个位置没有放置任何东西！', title:'空白'});

		console.log('all event ready ...');
		console.dir(_epcTypeList);
		// console.dir(_typeInfoList);
		console.dir(typeImageMap);
		res.render('carbinetFront4', 
			{//_minInventorySettings: JSON.stringify(minInventorySettings), 
				_epcTypeList: (_epcTypeList), //已经序列化过
				_typeImageMap: JSON.stringify(typeImageMap),
				_wsUriEventCenter: wsUriEventCenter, 
				title: '智能货架'});
	});
	
	ep.all('requestListEpc', function(_epcResponse){
		console.log('request epc list ...');	
		if(_epcResponse.statusCode == 200){
			console.log(_epcResponse.body);
			ep.emit('listEpc', _epcResponse.body);
		}else{
			ep.emit('error', 'requestListEpc error');
		}		
	});
	ep.all('requestListTypeInfo', function(_response){
		console.log('request type info list ...');
		if(_response.statusCode == 200){
			console.log(_response.body);
			ep.emit('listTypeInfo', _response.body);
		}else{
			ep.emit('error', 'requestListTypeInfo error');
		}
	});
	ep.fail(function(_err){
		console.log('carbinetFront4 error');
		res.render('carbinetFront4', 
			{//_minInventorySettings: JSON.stringify(minInventorySettings), 
				_epcTypeList: JSON.stringify([]), 
				_typeImageMap: JSON.stringify([]), 
				_wsUriEventCenter: wsUriEventCenter, 
				title: '智能货架'});
	});
	// httpRequestPost(EpcServerURL +'/productlist', ep.done('requestListEpc'));
	request(EpcServerURL +'/productlist', ep.done('requestListEpc'));
	request(EpcServerURL + '/listTypeInfo', ep.done('requestListTypeInfo'));		
	// res.render('carbinetFront4', {title: '智能货架'});
};
exports.carbinetFront5 = function(req, res){
	var typeImageMapList = [{itemType:'01', image:'xbox.png'}, {itemType:'null', image:'box1.png'}, 
							{itemType:'default', image:'default.png'}, {itemType:'02', image:'mac.png'}];

	// res.render('carbinetFront5', {title: '智能货架'});
	httpRequestPost(EpcServerURL +'/productlist', {}).then(function(_response){
		var _epcTypeList =  _response[0].body;
		return httpRequestPost(EpcServerURL + '/listTypeInfo', {}).then(function(_response){
			var  _typeInfoList =  _response[0].body;

			var typeImageMap = _.map(JSON.parse(_typeInfoList), function(_typeInfo){
				var imageMap = _.find(typeImageMapList, function(_map){
					return _map.itemType == _typeInfo.typeCode;
				});
				if(imageMap == null) imageMap = {itemType:'default', image:'default.png'};
				return {itemType: _typeInfo.typeCode, title: _typeInfo.typeName, des: _typeInfo.des, image: imageMap.image};
			});//需要序列化之后给模板
			typeImageMap.push({itemType:'default', image:'default.png', des:'这个位置放置的东西还没有制定种类！', title:'未知'});
			typeImageMap.push({itemType:'null', image:'box1.png', des:'这个位置没有放置任何东西！', title:'空白'});

			console.log('all event ready ...');
			console.dir(_epcTypeList);
			// console.dir(_typeInfoList);
			console.dir(typeImageMap);
			res.render('carbinetFront4', 
				{//_minInventorySettings: JSON.stringify(minInventorySettings), 
					_epcTypeList: (_epcTypeList), //已经序列化过
					_typeImageMap: JSON.stringify(typeImageMap),
					_wsUriEventCenter: wsUriEventCenter, 
					title: '智能货架'});


		})
	}).catch(function(error){
		console.log('error <= carbinetFront5 '.error);
	});
};




