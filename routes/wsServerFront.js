
var http = require('http');
var WebSocketServer = require('ws').Server;
var udpServer = require('./FrontTagLogic');
var _ = require("underscore");

var server;
global.wss;
var clients = [];

ep.tail('tagAdded', broadcastTagInfo);
ep.tail('tagDeleted', broadcastTagInfo);
ep.tail('tagAntenaChanged', broadcastTagInfo);

function broadcastTagInfo(_tags){
	// var obj = {name:'nodes', content:msg};
	// console.dir(_tags);
	global.wss.broadcast(JSON.stringify(_tags));
}

exports.startWebSocketServer = function(app){

	server = http.createServer(app);
	global.wss = new WebSocketServer({server : server});
	global.wss.broadcast = function(data) {
		// console.dir(clients);
		_.each(clients, function(_client){
	    	// console.dir(_client);
	        _client.send(data);
		});
	};
	wss.on('connection', function(ws){
		clients.push(ws);

	    console.log('connected');
	    // 连接成功后将当前所有存在的标签都发送给客户端
	    // var obj = {name:'nodes', content:JSON.stringify(global.nodeList)};
	    var tagList = udpServer.getAllExistTags();
	    var str = JSON.stringify(tagList);
	    console.log(str);
	    ws.send(str);

		ws.on('open', function(){
		    // console.log('connected');
		    // var obj = {name:'nodes', content:JSON.stringify(global.nodeList)};
		    // var str = JSON.stringify(obj);
		    // console.log(str);
		    // ws.send(str);
		  });
		
		ws.on('message', function(msg){
		    console.log('message => '+ msg);
		    var objMsg  = JSON.parse(msg);
		    // if(objMsg.command == 'alltags'){//接收到这个命令，将当前所有存在的标签都发送给客户端

		    // }
		    // wss.broadcast('broadcast => ' + msg);
		  });
		
		ws.on('close', function(){
		    var index = clients.indexOf(ws);
		    clients.splice(index, 1);
		    console.log('close =>');
		  });
		  
		});	

	return server;
}



