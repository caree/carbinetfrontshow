require('./config');

/**
 * **************************************************
 */
var Q                  = require('q');
var request = require('request');
 httpRequestGet = Q.denodeify(request.get);
httpRequestPost = Q.denodeify(request.post);

var EventProxy = require('eventproxy');
ep = new EventProxy();
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});

// require('./routes/simulator');
var express = require('express');
var http = require('http');
var path = require('path');
var routes = require('./routes');
var wsServer = require('./routes/wsServerFront');

var app = express();

var server = wsServer.startWebSocketServer(app);
// udpServer.startUDPListening();
// all environments
app.set('port', process.env.PORT || httpListeningPort);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/carbinetFront', routes.carbinetFront5);
app.get('/', routes.carbinetFront5);

server.listen(app.get('port'), function(){
  console.log(('前台货架服务，监听端口 ' + app.get('port')).info);
});
